# TradeSpecifix Coding Challenge

Coding challenge for TradeSpecifix - Full Stack Developer

Windows Operating system was used to complete this project

# Getting Started

tradespecifix folder contains the files related to the project.
Steps to start the application :

- Clone the repository.

- Open the tradespecifix folder in Android Studio.

- Connect to a emulator/android phone before running the application.

- Start the emulator from the ADB.

- Run the application

- Application starts in the emulator/phone.

Note : The project was developed in a Windows 10 Pro Operating System. It was tested on emulator(Pixel 2XL)


