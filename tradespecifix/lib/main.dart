import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Trade Specifix Challenge',
      theme: ThemeData(

        primarySwatch: Colors.blueGrey,

        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Edit Profile'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String _mySelection="";

  final String url = "https://api.printful.com/countries";
  List<dynamic>  data = List();
  bool isEnabled =false;

  Future<String> getCountryVal() async {
    var res = await http
        .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});

    Map<String, dynamic> map = json.decode(res.body);
//    data = map["countries"];
//    print(data[0]["name"]);
//    var resBody = json.decode(res.body);

    setState(() {
//      data = resBody;

      data = map["result"];
    });

    print(data);

    return "Success";
  }

  @override
  void initState() {
    super.initState();
    _mySelection = null;
    this.getCountryVal();
  }

  buttonEnable(){

    setState(() {
      isEnabled = false;
      print("dropdown value "+_mySelection);
    });
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

          Padding(
              padding: EdgeInsets.fromLTRB(15, 20, 15, 15),
              child: TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'First Name')
              ),
                  ),
            Padding(
              padding: EdgeInsets.fromLTRB(15, 20, 15, 15),
              child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Last Name',
                  )
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(15, 20, 15, 15),
              child: TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(

                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        borderSide: BorderSide(width: 2),
                      ),
                      labelText: 'E-Mail')
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
              child: TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password')
              ),
            ),


                Padding(
                    padding: EdgeInsets.fromLTRB(15, 7, 15, 15),
                    child:
                        Padding(
                            padding: EdgeInsets.fromLTRB(10,10, 10, 10),
                            child: new DropdownButton(
                              items: data.map((item) {


                                return new DropdownMenuItem(
                                  child: new Text(item['name']),
                                  value: item['code'].toString(),
                                 );
                              }).toList(),
                              onChanged: (newVal) {
                                setState(() {
                                  _mySelection = newVal;
                                });
                              },
                              value:_mySelection,
                            )

                        ),

                    ),
            Container(
                margin: EdgeInsets.all(15.0),
                padding: EdgeInsets.fromLTRB(5, 5, 5, 5),

                child: RaisedButton(
                  color: Colors.orangeAccent,
                    onPressed : (_mySelection=='CA') ? () =>  buttonEnable() : null,

                     child: Text('Change Password?')
                )
            ),
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
